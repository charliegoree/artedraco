import {writable} from "svelte/store";

export const docs = writable([]);
export const historial = writable([]);
export const kits = writable({Grande:0,Chico:0});
export const logueo = writable(false);
